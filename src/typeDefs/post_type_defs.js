const { gql } = require('apollo-server');
const postTypeDefs = gql `
type post {
    id: String!
    titlePost: String!
    descriptionPost: String!
    imagePost: String!
    datePost: String!
    idUserFK: String!
    username: String!
    nameCategory: String!
}
input postInput{
    titlePost: String!
    descriptionPost: String!
    imagePost: String!
    datePost: String!
    idUserFK: String!
    username: String!
    nameCategory: String!
}
extend type Query {
postByUsername(username: String!): post
}
extend type Mutation{
    createPost(postI : postInput!): post
}
`;
module.exports = postTypeDefs;