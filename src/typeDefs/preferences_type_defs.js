 const { gql } = require('apollo-server');
const preferencesTypeDefs = gql `
type Preferences { 
    enviado: Int!
    id: Int!
    idUserFK: Int!
    nameCategory: String!
}
input PreferencesInput{
    idUser: Int!
    CategoryName: String!
}
input PreferencesInputU {
    idUser: Int!
    CategoryName: String!
}
    
input PreferencesInputD {
    id: Int!
}
extend type Query {
PreferencesdDetailById(preferenceId: Int!): Preferences!
}
extend type Mutation {
createPreference(preferenceInput : PreferencesInput): Tokens!
updatePreference(preferencesI: PreferencesInputU!): Preferences!
deletePreference(preferencesD: PreferencesInputD!): Preferences!
}
`;
module.exports = preferencesTypeDefs;