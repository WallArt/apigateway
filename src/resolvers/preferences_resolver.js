const preferencesResolver = {
    Query: {
        PreferencesdDetailById: (_, { preferenceId }, { dataSources }) => {
            //if (userId == userIdToken)
            return await dataSources.authAPI.getPreference(preferenceId)
            //else
                //return null
            
            //},
        },
    },   
    Mutation: {
        createPreference: async(_, { preferenceInput }, { dataSources }) => {
            const accountInput = {
            idUser: preferenceInput.idUser,
            CategoryName: preferenceInput.CategoryName,
            lastChange:(new Date()).toISOString()
            }
            return await dataSources.accountAPI.createPreference(preferenceInput);
    
            },

            //preferenceDelete: (_, {preferencesD }, { dataSources }) =>{
            //dataSources.authAPI.deletePreferences(preferencesD),
    //
            //},
            PreferenceUpdate: (_, { preferencesI }, { dataSources }) =>{
                const authInput = {
                    idUser: preferenceInput.idUser,
                    CategoryName: preferenceInput.CategoryName,
                }
                return await dataSources.authAPI.updatePreferencw(authInput);
            }
        }
    };
    module.exports = preferencesResolver;