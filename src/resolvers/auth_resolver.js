const authResolver = {
Query: {
    userDetailById: (_, { userId }, { dataSources, userIdToken }) => {
        if (userId == userIdToken)
            return dataSources.authAPI.getUser(userId)
        else
            return null
        
        },
    },
    
Mutation: {
    signUpUser: async(_, { userInput }, { dataSources }) => {
        const authInput = {
            nameUser: userInput.nameUser,
            lastNameUser: userInput.lastNameUser,
            username: userInput.username,
            password: userInput.password,
            emailUser: userInput.emailUser,
            cellphoneUser: userInput.cellphoneUser,
            dateBirthUser: userInput.dateBirthUser,
            genderUser: userInput.genderUser,
            statusUser: userInput.statusUser,
            userType: userInput.userType,
            imageUser: userInput.imageUser
            }
            return await dataSources.authAPI.createUser(authInput);
        },
        logIn: (_, { credentials }, { dataSources }) =>
            dataSources.authAPI.authRequest(credentials),
    
        refreshToken: (_, { refresh }, { dataSources }) =>
            dataSources.authAPI.refreshToken(refresh),

        UserDelete: (_, { DeleteU }, { dataSources }) =>
        dataSources.authAPI.deleteUser(DeleteU),

        
        UserUpdate: (_, { UpdateU }, { dataSources }) =>{
        const authInput = {
            nameUser: UpdateU.nameUser,
            lastNameUser: UpdateU.lastNameUser,
            username: UpdateU.username,
            password: UpdateU.password,
            emailUser: UpdateU.emailUser,
            cellphoneUser: UpdateUcellphoneUser,
            dateBirthUser: UpdateU.dateBirthUser,
            genderUser: UpdateU.genderUser,
            statusUser: UpdateU.statusUser,
            useType: UpdateU.useType,
            imageUser: UpdateU.imageUser
        }
        return  dataSources.authAPI.updateUser(authInput);
        
    }
}
    };
module.exports = authResolver;